
type chain = {
  mutable nodes: Vec2.vec2 list;
  mutable bones: float list;
};;


let create () = {
  nodes = ([] : Vec2.vec2 list);
  bones = ([] : float list);
};;


let rec populate chain size =
  match size with
  | 0 -> ignore()
  | _ ->
    chain.nodes <- (Vec2.create
      (Random.float 640.0)
      (Random.float 460.0)) :: chain.nodes;
    match size with
    | 1 -> ignore()
    | _ ->
      chain.bones <- 25.0 :: chain.bones;
      populate chain (size - 1)
;;


let rec print (nodes : Vec2.vec2 list) =
  print_endline "print function";
  match nodes with
  | [] -> ignore()
  | node_to_print :: other_nodes ->
    print_string "[ x: ";
    print_float node_to_print.x;
    print_string "; y: ";
    print_float node_to_print.y;
    print_endline " ]";
    print other_nodes
;;


let rec render_bones (nodes : Vec2.vec2 list) =
  match nodes with
  | [] | [_] -> ignore()
  | bone_start :: bone_end :: other_nodes ->
    Raylib.draw_line
      (int_of_float bone_start.x)
      (int_of_float bone_start.y)
      (int_of_float bone_end.x)
      (int_of_float bone_end.y)
      Raylib.Color.red;
    render_bones (bone_end :: other_nodes)
;;

let rec render_nodes (nodes : Vec2.vec2 list) =
  match nodes with
  | [] -> ignore()
  | node_to_render :: other_nodes ->
    Raylib.draw_circle
      (int_of_float node_to_render.x)
      (int_of_float node_to_render.y)
      4.0 Raylib.Color.blue;
    render_nodes other_nodes
;;


let render chain =
  render_bones chain.nodes;
  render_nodes chain.nodes
;;
