
(*
  Before                |  After_____-x.
      x-------x         |    x``         \..    
     /        |         |    /              \.  
    /         |      o  |   /                 0
   /          |         |   x
  x           x         |
*)
let rec forward_pass nodes bones target =
  match bones with
  | [] -> []
  | bone_length :: leftover_bones ->
    match nodes with
    | [] -> []
    | node_to_move :: leftover_nodes ->
      let from_target_to_next_target = Vec2.substract node_to_move target in
      let to_next_target_direction = Vec2.normalized from_target_to_next_target in
      let next_target_position_from_target = Vec2.multiply to_next_target_direction bone_length in
      let next_target_position = Vec2.add next_target_position_from_target target in
      (forward_pass leftover_nodes leftover_bones next_target_position) @ [target]
;;


let rec resolve nodes bones target times =
  if times > 0 then
    match (List.rev nodes) with
    | [] -> []
    | next_target :: _ ->
      let new_nodes = forward_pass nodes bones target in
      resolve new_nodes (List.rev bones) next_target (times - 1)
  else
    nodes
;;
