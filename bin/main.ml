
let setup () =
  Raylib.init_window 640 460 "Ocaml FABRIK";
  Raylib.set_target_fps 100
;;


let update (chain : Chain.chain ref) =
  (!chain).nodes <- Fabrik.resolve
    (!chain).nodes
    (!chain).bones
    (Vec2.create
      (float_of_int (Raylib.get_mouse_x ()))
      (float_of_int (Raylib.get_mouse_y ())))
    20
;;

let render chain =
  Raylib.begin_drawing ();
    Raylib.clear_background Raylib.Color.black;
    Chain.render chain;
  Raylib.end_drawing ();
;;

let rec loop chain =
  match Raylib.window_should_close () with
  | true -> Raylib.close_window()
  | false ->
    update chain;
    render !chain;
    loop chain
;;


let () =
  Random.self_init ();
  let chain = ref (Chain.create ()) in
  Chain.populate !chain 20;
  setup();
  loop chain;
;;
