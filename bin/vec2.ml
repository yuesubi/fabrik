type vec2 = {
  mutable x: float;
  mutable y: float;
};;

let create xPos yPos = {
  x = xPos;
  y = yPos;
}

let add vector0 vector1 = {
  x = vector0.x +. vector1.x;
  y = vector0.y +. vector1.y;
};;

let substract vector0 vector1 = {
  x = vector0.x -. vector1.x;
  y = vector0.y -. vector1.y;
};;

let multiply vec2 scalar = {
  x = vec2.x *. scalar;
  y = vec2.y *. scalar;
};;

let divide vec2 inv_scalar = {
  x = vec2.x /. inv_scalar;
  y = vec2.y /. inv_scalar;
};;

let squared_length vec2 =
  vec2.x *. vec2.x +.
  vec2.y *. vec2.y
;;

let length vec2 =
  sqrt (squared_length vec2)
;;

let normalized vec2 =
  divide vec2 (length vec2)
;;