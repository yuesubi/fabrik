# Ocaml FABRIK
Forward and backwards reaching inverse kinematics in OCaml

## Run (Linux)
```sh
opam install raylib

git clone https://gitlab.com/yuesubi/fabrik.git
cd fabrik

dune build
dune exec fabrik
```
